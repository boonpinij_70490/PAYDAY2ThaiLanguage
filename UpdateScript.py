# blt_hasher by shinrax2
# zip and meta.json generate by CENSOR_1337

import hashlib, os, shutil, json



def getfiles(dirpath):
    f =  []
    for root, dirs, files in os.walk(dirpath):
        for file in files:
            if os.path.isfile(os.path.join(root, file)):
                f.append(os.path.join(root, file))
    f.sort(key=str.lower)
    return f

def hashl(line):
    hash = hashlib.sha256()
    hash.update(line)
    return hash.hexdigest()

def hashit(file, block_size=65536):
    hash = hashlib.sha256()
    with open(file, "rb") as f:
        for line in iter(lambda: f.read(block_size), b''):
            hash.update(line)
    return hash.hexdigest()



if __name__ == "__main__":

    import argparse
    hash = ''
    parser = argparse.ArgumentParser(description="Enter path for the folder you want to have hashed(sha256)")
    parser.add_argument('path', metavar='path', type=str, nargs=1, help='path for the folder to be hashed')
    arg = parser.parse_args()
    if arg.path:
        hashstr = ""
        files = getfiles(arg.path[0])
        for file in files:
            hash = hashit(file)
            hashstr = hashstr + hash
        result = hashl(bytes(hashstr, 'ascii'))
        print(result)

# path manager

default_path = 'D:/00_Git/censor1337.gitlab.io/PAYDAY2/PAYDAY2ThaiLanguage/'


meta_json_path = default_path + 'meta.json'
zip_output_path = default_path + 'PD2TH'

ident = 'PD2TH'
hash = result
patchnotes_url = 'https://censor1337.gitlab.io/PAYDAY2/PAYDAY2ThaiLanguage/patchnotes/'
download_url = 'https://censor1337.gitlab.io/PAYDAY2/PAYDAY2ThaiLanguage/PD2TH.zip'

update_meta = []
update_info_meta = {}

update_info_meta ["ident"] = ident
update_info_meta ["hash"] = hash
update_info_meta ["patchnotes_url"] = patchnotes_url
update_info_meta ["download_url"] = download_url

update_meta.append(update_info_meta)

jstring = json.dumps(update_meta, sort_keys=False, indent=4)


OutFile = open(meta_json_path, "w+")
OutFile.write(jstring)
OutFile.close()

zip_mod_dir = "./mods/"
shutil.make_archive(zip_output_path, 'zip', zip_mod_dir,"PD2TH/")